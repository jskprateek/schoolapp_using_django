from django.views.generic import *
from django.db.models import *
from django.contrib.auth import authenticate, login, logout
from django.contrib.auth.mixins import LoginRequiredMixin
from .forms import *
from django.shortcuts import *
from .models import *
from .gpa import FinalGrades
from reportlab.pdfgen import canvas
from django.http import HttpResponse
import pdfkit


class StudentDataMixin(object):
    def get_context_data(self, *args, **kwargs):
        context = super().get_context_data(*args, **kwargs)
        context['marks'] = Mark.objects.all()
        context['allgrades'] = Grade.objects.all()
        return context


class HomeView(LoginRequiredMixin, StudentDataMixin, TemplateView):
    template_name = 'home.html'
    login_url = '/login/'


class LoginView(FormView):
    template_name = 'login.html'
    form_class = LoginForm
    success_url = '/'

    def get(self, request):
        if request.user.is_authenticated:
            return render(self.request, 'home.html')
        return super().get(request)

    def form_valid(self, form):
        username = form.cleaned_data['username']
        password = form.cleaned_data['password']
        user = authenticate(username=username,
                            password=password)
        if user is not None and user.is_superuser:
            login(self.request, user)
        else:
            return render(self.request, self.template_name, {
                'form': form,
                'errors': "Incorrect Credentials"})
        return super().form_valid(form)

    def get_success_url(self, *args, **kwargs):
        self.next_url = self.request.POST.get('next')
        if self.next_url is not None:
            return self.next_url
        else:
            return self.success_url


class LogoutView(LoginRequiredMixin, View):

    def get(self, request):
        if request.user.is_authenticated:
            logout(request)
        return redirect('/login/')


class GradeDetailView(LoginRequiredMixin, StudentDataMixin,
                      DetailView):
    template_name = 'gradedetail.html'
    model = Grade
    context_object_name = 'selectedgrade'
    login_url = '/login/'

    def get_context_data(self, *args, **kwargs):
        context = super().get_context_data(*args, **kwargs)
        self.id = self.kwargs['pk']
        return context


class StudentDetailView(LoginRequiredMixin, StudentDataMixin, DetailView):
    template_name = 'studentdetail.html'
    model = Student
    context_object_name = 'student'
    login_url = '/login/'

    def get_context_data(self, *args, **kwargs):
        context = super().get_context_data(*args, **kwargs)
        studentmarks = Mark.objects.filter(student__id=self.kwargs['pk'])
        gradepoints = []
        for marks in studentmarks:
            gradepoints.append(marks.gradepoint)
        totalcredits = sum(gradepoints)
        try:
            gpa = round(totalcredits / len(gradepoints), 2)
        except Exception:
            gpa = 0

        final_grade = FinalGrades()
        final_grade.calculategrades(gpa)

        context['studentmarks'] = studentmarks
        context['final_gradeletter'] = final_grade.gradeletter
        context['final_remark'] = final_grade.graderemark
        context['gpa'] = gpa
        return context


class ClassTeachersListView(LoginRequiredMixin, ListView):
    template_name = 'classteacherslist.html'
    model = ClassTeacher
    context_object_name = 'classteacherslist'
    login_url = '/login/'


class AddClassTeacherView(LoginRequiredMixin, FormView):
    template_name = 'addclassteacher.html'
    model = ClassTeacher
    form_class = AddClassTeacherForm
    success_url = '/'

    def form_valid(self, form):
        self.name = form.cleaned_data['name']
        ClassTeacher.objects.create(name=self.name)
        return super().form_valid(form)

    def get_success_url(self, *args, **kwargs):
        redirect_url = '/classteacherslist/'
        return redirect_url


class AddGradesView(LoginRequiredMixin, FormView):
    template_name = 'addgrades.html'
    model = Grade
    form_class = AddGradesForm
    success_url = '/'

    def form_valid(self, form, **kwargs):
        self.grade = form.cleaned_data['grade']
        self.grade_id = self.kwargs['g_id']
        self.grade = Grade.objects.get(id=self.grade_id)
        self.class_teacher = form.cleaned_data['class_teacher']
        Grade.objects.update_or_create(
            grade=self.grade, class_teacher=self.class_teacher)
        return super().form_valid(form)

    def get_success_url(self, *args, **kwargs):
        redirect_url = '/grade/' + str(self.grade_id) + '/gradedetail/'
        return redirect_url


class AddStudentsView(LoginRequiredMixin, FormView):
    template_name = 'addstudents.html'
    model = Student
    form_class = AddStudentsForm
    success_url = '/'

    def form_valid(self, form):
        self.name = form.cleaned_data['name']
        self.grade_id = self.kwargs['g_id']
        self.grade = Grade.objects.get(id=self.grade_id)
        Student.objects.create(
            name=self.name, grade=self.grade)
        return super().form_valid(form)

    def get_success_url(self, *args, **kwargs):
        redirect_url = '/grade/' + str(self.grade_id) + '/gradedetail/'
        return redirect_url


class AddSubjectsView(LoginRequiredMixin, FormView):
    template_name = 'addsubjects.html'
    form_class = AddSubjectsForm
    success_url = '/'

    def form_valid(self, form):
        self.title = form.cleaned_data['title']
        self.grade_id = self.kwargs['g_id']
        self.grade = Grade.objects.get(id=self.grade_id)
        Subject.objects.create(
            title=self.title, grade=self.grade)
        return super().form_valid(form)

    def get_success_url(self, *args, **kwargs):
        redirect_url = '/grade/' + str(self.grade_id) + '/gradedetail/'
        return redirect_url


class GeneratePdfView(View):
    def get(self, request):
        response = HttpResponse(content_type='application/pdf')
        response['Content-Disposition'] = 'attachment; \
        filename="student_marksheet.pdf"'

        # Create the PDF object, using the response object as its "file."
        p = canvas.Canvas(response)
        # input_filename = 'studentdetail.html'
        # output_filename = 'student_marksheet.pdf'

        # with open(input_filename, 'r') as f:
        #     html_text = f.read()

        # data = pdfkit.from_string(html_text, output_filename)
        # Draw things on the PDF. Here's where the PDF generation happens.
        # See the ReportLab documentation for the full list of functionality.
        data = 'Hello World!'
        p.drawString(100, 100, data)

        # Close the PDF object cleanly, and we're done.
        p.showPage()
        p.save()
        return response
