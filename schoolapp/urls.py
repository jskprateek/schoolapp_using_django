from django.urls import path
from .views import *
# from schoolapp import views

app_name = 'schoolapp'

urlpatterns = [
    path('', HomeView.as_view(), name='home'),
    path('login/', LoginView.as_view(), name='login'),
    path('logout/', LogoutView.as_view(), name='logout'),


    # Grade Detail
    path('grade/<int:pk>/gradedetail/',
         GradeDetailView.as_view(), name='gradedetail'),

    # Student Detail
    path('grade/<int:g_id>/student/<int:pk>/studentdetail/',
         StudentDetailView.as_view(), name='studentdetail'),

    # Class Teachers List
    path('classteacherslist/', ClassTeachersListView.as_view(),
         name='classteacherslist'),



    # Adding Data

    # Add Class Teacher
    path('addclassteacher/',
         AddClassTeacherView.as_view(), name='addclassteacher'),

    # Add Grades
    path('addgrades/',
         AddGradesView.as_view(), name='addgrades'),

    # Add Students
    path('grade/<int:g_id>/addstudents/',
         AddStudentsView.as_view(), name='addstudents'),

    # Add Sujects
    path('grade/<int:g_id>/addsubjects/',
         AddSubjectsView.as_view(), name='addsubjects'),

    # Generate StudentDetail Pdf
    path('studentdetail/generatepdf',
         GeneratePdfView.as_view(),
         name='generatepdf'),

]
