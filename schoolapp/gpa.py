class FinalGrades:
    gradeletter = ""
    graderemark = ""

    def calculategrades(self, gpa):
        if 3.6 <= gpa <= 4.0:
            gr = "A+"
            remark = "Outstanding"
        elif 3.2 <= gpa < 3.6:
            gr = "A"
            remark = "Excellent"
        elif 2.8 <= gpa < 3.2:
            gr = "B+"
            remark = "Very Good"
        elif 2.4 <= gpa < 2.8:
            gr = "B"
            remark = "Good"
        elif 2.0 <= gpa < 2.4:
            gr = "C+"
            remark = "Above Average"
        elif 1.6 <= gpa < 2.0:
            gr = "C"
            remark = "Average"
        elif 0.8 <= gpa < 1.6:
            gr = "D"
            remark = "Below Average"
        elif 0 <= gpa < 0.8:
            gr = "E"
            remark = "Insufficient"
        self.gradeletter = gr
        self.graderemark = remark
        return
