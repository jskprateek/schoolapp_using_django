from django.db import models
from django.core.validators import MinValueValidator, MaxValueValidator


class ClassTeacher(models.Model):
    name = models.CharField(max_length=100)

    def __str__(self):
        return self.name


class Grade(models.Model):
    grade = models.PositiveIntegerField(unique=True)
    class_teacher = models.OneToOneField(
        ClassTeacher, on_delete=models.CASCADE)

    def __str__(self):
        return str(self.grade)


class Subject(models.Model):
    title = models.CharField(max_length=100)
    # credits = models.PositiveSmallIntegerField(
    #     validators=[MinValueValidator(1), MaxValueValidator(4)])
    grade = models.ForeignKey(
        Grade, on_delete=models.CASCADE, related_name='subjects')

    def __str__(self):
        return self.title


class Student(models.Model):
    name = models.CharField(max_length=100)
    grade = models.ForeignKey(
        Grade, on_delete=models.CASCADE, related_name='students')

    def __str__(self):
        return self.name


class Mark(models.Model):
    marks = models.PositiveIntegerField()
    gradepoint = models.DecimalField(
        null=True, blank=True, max_digits=2, decimal_places=1,
        validators=[MinValueValidator(1), MaxValueValidator(4)])
    gradeletter = models.CharField(null=True, blank=True, max_length=2)
    remark = models.CharField(null=True, blank=True, max_length=30)
    subject = models.ForeignKey(
        Subject, on_delete=models.CASCADE, related_name='marks')
    student = models.ForeignKey(
        Student, on_delete=models.CASCADE)

    def __str__(self):
        return str(self.marks)

    def save(self, *args, **kwargs):
        if 90 <= self.marks <= 100:
            self.gradeletter = "A+"
            self.gradepoint = 4.0
            self.remark = "Outstanding"
        elif 80 <= self.marks < 90:
            self.gradeletter = "A"
            self.gradepoint = 3.6
            self.remark = "Excellent"
        elif 70 <= self.marks < 80:
            self.gradeletter = "B+"
            self.gradepoint = 3.2
            self.remark = "Very Good"
        elif 60 <= self.marks < 70:
            self.gradeletter = "B"
            self.gradepoint = 2.8
            self.remark = "Good"
        elif 50 <= self.marks < 60:
            self.gradeletter = "C+"
            self.gradepoint = 2.4
            self.remark = "Above Average"
        elif 40 <= self.marks < 50:
            self.gradeletter = "C"
            self.gradepoint = 2.0
            self.remark = "Average"
        elif 20 <= self.marks < 40:
            self.gradeletter = "D"
            self.gradepoint = 1.6
            self.remark = "Below Average"
        elif 1 <= self.marks < 20:
            self.gradeletter = "E"
            self.gradepoint = 0.8
            self.remark = "Insufficient"
        elif self.marks == 0:
            self.gradeletter = "N"
            self.gradepoint = 0
            self.remark = "Not Graded"
        super(Mark, self).save(*args, **kwargs)
