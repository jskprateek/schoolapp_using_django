# Generated by Django 2.0.5 on 2018-05-03 06:34

from django.db import migrations, models
import django.db.models.deletion


class Migration(migrations.Migration):

    dependencies = [
        ('schoolapp', '0007_auto_20180503_1124'),
    ]

    operations = [
        migrations.AlterField(
            model_name='student',
            name='grade',
            field=models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, related_name='student', to='schoolapp.Grade'),
        ),
    ]
