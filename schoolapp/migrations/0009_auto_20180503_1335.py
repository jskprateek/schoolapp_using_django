# Generated by Django 2.0.5 on 2018-05-03 07:50

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('schoolapp', '0008_auto_20180503_1219'),
    ]

    operations = [
        migrations.AlterField(
            model_name='mark',
            name='student',
            field=models.ManyToManyField(related_name='marks', to='schoolapp.Student'),
        ),
        migrations.AlterField(
            model_name='subject',
            name='grade',
            field=models.ManyToManyField(related_name='subject', to='schoolapp.Grade'),
        ),
    ]
