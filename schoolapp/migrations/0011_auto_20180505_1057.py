# Generated by Django 2.0.5 on 2018-05-05 05:12

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('schoolapp', '0010_auto_20180503_1358'),
    ]

    operations = [
        migrations.AlterField(
            model_name='mark',
            name='student',
            field=models.ManyToManyField(related_name='student_marks', to='schoolapp.Student'),
        ),
        migrations.AlterField(
            model_name='mark',
            name='subject',
            field=models.ManyToManyField(related_name='subject_marks', to='schoolapp.Subject'),
        ),
    ]
