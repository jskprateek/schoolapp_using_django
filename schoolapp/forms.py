from django import forms
from .models import ClassTeacher

classteacher_choices = []
for teacher in ClassTeacher.objects.all():
    classteacher_choices.append(teacher.name)
print(classteacher_choices)


class LoginForm(forms.Form):
    username = forms.CharField(widget=forms.TextInput(attrs={
        'class': 'form-control error'
    }))
    password = forms.CharField(widget=forms.PasswordInput(attrs={
        'class': 'form-control error'
    }))


class AddClassTeacherForm(forms.Form):

    name = forms.CharField(widget=forms.TextInput(attrs={
        'class': 'form-control error'
    }))


class AddGradesForm(forms.Form):

    grade = forms.CharField(widget=forms.TextInput(attrs={
        'class': 'form-control error'
    }))
    class_teacher = forms.ModelChoiceField(required=True,
                                           empty_label='---Select Teacher---',
                                           queryset=ClassTeacher.objects.all())

    class Meta:
        model = ClassTeacher


class AddStudentsForm(forms.Form):

    name = forms.CharField(widget=forms.TextInput(attrs={
        'class': 'form-control error'
    }))


class AddSubjectsForm(forms.Form):

    title = forms.CharField(widget=forms.TextInput(attrs={
        'class': 'form-control error'
    }))
